// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.3.0

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;

namespace SimpleEchoBot.Bots
{
    public class EchoBot : ActivityHandler
    {
        private readonly ILogger<EchoBot> _logger;

        public EchoBot(
            ILogger<EchoBot> logger
            )
        {
            _logger = logger;
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            IMessageActivity activity = turnContext.Activity.AsMessageActivity();

            _logger.LogDebug($"Entered EchoBot.OnMessageActivityAsync: {activity.Text}");

            switch (activity.Text)
            {
                case "await":
                    _logger.LogDebug("Sending 10 awaited responses");
                    await SendAwaited(turnContext, cancellationToken);
                    break;
                default:
                    _logger.LogDebug("Sending 10 unawaited responses");
                    SendUnawaited(turnContext, cancellationToken);
                    break;
            }

        }

        private void SendUnawaited(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            Activity[] response = new Activity[10];
            for (int i = 0; i < response.Length; i++)
            {
                response[i] = MessageFactory.Text($"-- Response {i + 1}");
            }

            Task _ = turnContext.SendActivitiesAsync(response, cancellationToken);
        }

        private Task SendAwaited(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            Activity[] response = new Activity[10];
            for (int i = 0; i < response.Length; i++)
            {
                response[i] = MessageFactory.Text($"-- Response {i + 1}");
            }

            return turnContext.SendActivitiesAsync(response, cancellationToken);
        }
    }
}
